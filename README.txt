
README.txt - version 2.0
-------------------------
Emma Block is a module for Drupal 6.x and 7.x that provides integration with myemma.com
mailing lists via version 2 of the Emma API. It creates a block that can be
assigned to any region which will take newsletter sign-ups from users without
the need for them to create accounts. Here is some of the functionality:

  * Ability to specify multiple mailing lists and have the user select which 
    they wish to register for or allow them to register for all at once.
  * Confirms email subscriptions via Emma to make it compliant with 
    double opt-in requirements.
  * Custom "Member Fields" possible with custom module's hook_form_FORMID_alter of
    the subscription form (MODULENAME_form_emma_block_subscribe_form_alter). To
    figure out the fields' machine names, visit or curl
    https://PUBAPIKEY:PRIVAPIKEY@api.e2ma.net/ACCOUNT_ID/fields
    to see a list. See API.txt for hints on adding fields.

Emma Block was written by Michael R. Bagnall (ElusiveMind). Contact me at
http://drupal.org/user/350597/contact or http://www.elusivemind.com

There are no dependencies for this module. This module does require that 
cURL be installed and added to PHP.

